<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\CategoryRepository;
use View;

class SettingController extends Controller
{
    protected $user;
    protected $category;

    public function __construct(UserRepository $user, CategoryRepository $category)
    {
        $this->user = $user;
        $this->category = $category;

        // Sharing is data to all views
        View::share('categories', $this->category->all());
    }

    /**
     * Display the current user setting list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setting.index');
    }

    /**
     * Display the notification page for setting.
     *
     * @return \Illuminate\Http\Response
     */
    public function notification()
    {
        return view('setting.notification');
    }

    /**
     * Set the email notification.
     *
     * @param Request $request [description]
     * @return  Redirect
     */
    public function setNotification(Request $request)
    {
        $input = [
            'email_notify_enabled' => $request->get('email_notify_enabled') ? 'yes' : 'no'
        ];

        $this->user->update(\Auth::id(), $input);

        return redirect()->back();
    }

    /**
     * Display the bindings page.
     *
     * @return \Illuminate\Http\Response
     */
    public function binding()
    {
        return view('setting.binding');
    }
}
