<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use View;

class ArticleController extends Controller
{
    protected $article;
    protected $category;

    public function __construct(ArticleRepository $article, CategoryRepository $category)
    {
        $this->article = $article;
        $this->category = $category;

        // Sharing is data to all views
        View::share('categories', $this->category->all());
    }

    /**
     * Display the articles resource.
     *
     * @return mixed
     */
    public function index()
    {
        $articles = $this->article->page(config('blog.article.number'), config('blog.article.sort'), config('blog.article.sortColumn'));

        return view('article.index', compact(['articles']));
    }

    /**
     * Display the article resource by article slug.
     *
     * @param  string $slug
     * @return mixed
     */
    public function show($slug)
    {
        $article = $this->article->getBySlug($slug);

//        $article->content = collect(json_decode($article->content))->get('html');


        return view('article.show', compact('article'));
    }
}
