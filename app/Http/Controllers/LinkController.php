<?php

namespace App\Http\Controllers;

use App\Repositories\LinkRepository;
use App\Repositories\CategoryRepository;
use View;

class LinkController extends Controller
{
    protected $link;

    public function __construct(LinkRepository $link, CategoryRepository $category)
    {
        $this->link = $link;
        $this->category = $category;

        // Sharing is data to all views
        View::share('categories', $this->category->all());
    }

    /**
     * Display the link resource.
     *
     * @return mix
     */
    public function index()
    {
        $links = $this->link->page();

        return view('link.index', compact('links'));
    }
}
