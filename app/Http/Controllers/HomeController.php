<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use View;

class HomeController extends Controller
{
    protected $article;
    protected $category;

    public function __construct(ArticleRepository $article, CategoryRepository $category)
    {
        $this->article = $article;
        $this->category = $category;

        // Sharing is data to all views
        View::share('categories', $this->category->all());
    }

    /**
     * Display the dashboard page.
     *
     * @return mixed
     */
    public function dashboard()
    {
        return view('dashboard.index');
    }

    /**
     * Search the article by keyword.
     *
     * @param  Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $key = trim($request->get('q'));

        $articles = $this->article->search($key);

        return view('search', compact('articles'));
    }
}
