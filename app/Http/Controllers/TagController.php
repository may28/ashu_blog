<?php

namespace App\Http\Controllers;

use App\Repositories\TagRepository;
use App\Repositories\CategoryRepository;
use View;

class TagController extends Controller
{
    protected $tag;
    protected $category;

    public function __construct(TagRepository $tag, CategoryRepository $category)
    {
        $this->tag = $tag;
        $this->category = $category;

        // Sharing is data to all views
        View::share('categories', $this->category->all());
    }

    /**
     * Display the tag resource.
     *
     * @return mixed
     */
    public function index()
    {
        $tags = $this->tag->all();

        return view('tag.index', compact('tags'));
    }

    /**
     * Display the articles and discussions by the tag.
     *
     * @param  string $tag
     * @return mixed
     */
    public function show($tag)
    {
        $tag = $this->tag->getByName($tag);

        if (!$tag) {
            abort(404);
        }

        $articles = $tag->articles;
        $discussions = $tag->discussions;

        return view('tag.show', compact('tag', 'articles', 'discussions'));
    }
}
