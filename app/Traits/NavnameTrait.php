<?php
/*
 * This file is for top nav name
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace App\Traits;

/**
 * Trait FollowTrait.
 */
trait NavnameTrait
{
    /**
     * Return all category names.
     *
     * @param null
     *
     * @return array
     */
    public function getAllCategoryNames($category)
    {
        return $this->category->all();
    }
}
