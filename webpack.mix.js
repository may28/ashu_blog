const { mix } = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
  'public/resume/css/bootstrap.min.css',
  'public/resume/css/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
  'public/resume/css/fonts/fontello/css/fontello.css',
  'public/resume/js/nprogress/nprogress.css',
  'public/resume/js/jquery.magnific-popup/magnific-popup.css',
  'public/resume/js/jquery.uniform/uniform.default.css',
  'public/resume/css/animations.css',
  'public/resume/css/align.min.css',
  'public/resume/css/main.min.css',
  'public/resume/css/768.min.css',
  'public/resume/css/style.css',
], 'public/resume/css/vendor.css').version();

mix.scripts([
  'public/resume/js/jquery-1.12.1.min.js',
  'public/resume/js/jquery-migrate-1.2.1.min.js',
  'public/resume/js/modernizr.min.js',
], 'public/resume/js/vendor-init.js').version();

mix.scripts([
  'public/resume/js/jquery.address-1.5.min.js',
  'public/resume/js/nprogress/nprogress.js',
  'public/resume/js/fastclick.js',
  'public/resume/js/typist.js',
  'public/resume/js/imagesloaded.pkgd.min.js',
  'public/resume/js/jquery.isotope.min.js',
  'public/resume/js/jquery.fitvids.js',
  'public/resume/js/jquery.validate.min.js',
  'public/resume/js/jquery.uniform/jquery.uniform.min.js',
  'public/resume/js/jquery.magnific-popup/jquery.magnific-popup.min.js',
  'public/resume/js/socialstream.jquery.js',
  'public/resume/js/jarallax.min.js',
  'public/resume/js/jarallax-video.min.js',
  'public/resume/js/main.js',
], 'public/resume/js/vendor.js').version();
