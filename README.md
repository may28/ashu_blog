# ashu_blog
A blog built with Laravel  + Vue 博客系统

### Basic Features

Manage users, articles, discussions and media
Statistical tables
Categorize articles
Label classification
Content moderation
Own comments system
Multi-language switching
Markdown Editor
and more...

### Server Requirements

PHP >= 7.0.0
Node >= 6.x
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension


:deciduous_tree: Ashu Blog, a multi-language & multi-user blog platform, built with Laravel and Vue.js.

# AHSU BLOG 2017.7
#### 注意： 本项目为规范流程项目，不是造轮子。 :high_brightness:
### 需求->原型->开发->测试->发布
### Requirements -> Prototypes -> Development -> Test -> Deploy
![workflow](https://s-media-cache-ak0.pinimg.com/originals/5f/31/18/5f3118456e0ac38a4177f9875c99a09a.png)

## 1.需求 Discovery Phase
a. 这个项目是个人全栈开发项目，目的是开发一个类似于wordpress的单用户博客系统。
b. 文章可以转发到第三方平台（FB,WEIBO,TWITTER, 微信等等）。
c. 管理员有专用后台，用来进行文章管理，文件管理和数据分析等。
d. 有数据分析(接入谷歌分析系统)。
e. V2期有微信公众号关联功能，用户可用微信登录，可以管理同步微信文章和管理微信粉丝
f. * 可以有email订阅功能。


## 2.设计 Design Phase ( Wireframing, Prototype 原型)
figma.com 个人版

## 3.开发 Development
### 3.1 Technology stack:

a.代码库，服务器
b.前后端框架
````
 - Laravel5.* + Vue.js 2.* +Bootstrap 3
 ````

c. ER-Diagram Design 数据库设计  [Design Tool：OmniGraffle](https://www.omnigroup.com/omniGraffle/)
### 3.2.开发计划 （按模块->前后端同步）
功能列表
````
  Todo
````
开发计划
````
  Todo
````
### e.项目管理 [Tool: trello.com](http://trello.com)


## 4.测试 QA / Testing
（Todo）

## 5.发布 Deployment

总结（Todo）

## :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree: - :deciduous_tree:

## 踩坑记录
- 修改.env后，需要执行清理缓存：
````
php artisan config:cache
````


## About Author
The project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

wechat: ashucn
email: ashucn@gmail.com
