<section id="resume" class="pt-page page-layout">
    <!-- .content -->
    <div class="content">
        <!-- .layout-medium -->
        <div class="layout-medium">

            <!-- page-title -->
            <h1 class="page-title">
                <i class="pe-7s-id"></i>@lang('resume.resume')
            </h1>
            <!-- page-title -->

            <!-- row -->
            <div class="row last-row">

                <!-- col -->
                <div class="col-sm-7">

                    <div class="event">
                        <h2>WORK HISTORY IN US</h2>
                        <p>
                            <i class="pe-7s-ribbon"></i>
                        </p>
                    </div>

                    <div class="event">
                        <h3>Jun 2019 - Present</h3>
                        <h4>UI Software Engineer</h4>
                        <h5>BayOne Solutions (provide services to StubHub, Inc)</h5>
                        <p>• Work as a UI software engineer (ReactJs tech stack), while at BayOne Solutions, provide services to StubHub, Inc (an eBay company) on a daily
                            basis.</p>
                        <p>• Develop ReactJs components of an enterprise-level system in close cooperation with the back-end team of
                            JAVA developers.</p>
                        <p>• Write and style the components in HTML5, CSS3 and JavaScript (ES6) that meet the requirements of our UX/UI designer’s mockup and fulfill our user
                            stories.</p>
                        <p>• Monitor and process pull requests for production deployments.</p>
                        <p>• Use Jest and testing-library for mocking functions and write unit tests for testing component coverage reports.</p>
                    </div>

                    <div class="event">
                        <h3>Dec 2017 - Jun 2019</h3>
                        <h4>Full Stack Web Development Manager</h4>
                        <h5>Unitedtars International Ltd. </h5>
                        <p>• Designed and developed the overall architecture of three web applications with a group of developers (usitour.com, hotel.usitrip.com, 117book.com).</p>
                        <p>• Led a group of 3 front-end developers and 4 back-end developers to create an online travel agency in VueJs, Laravel, Redis, MySQL and AWS.</p>
                        <p>• Played a hands-on development and design role, and delivered products in a rapid and dynamic environment.
                        </p>
                    </div>

                    <div class="event">
                        <h3>Aug 2015 - Dec 2017</h3>
                        <h4>Full Stack Web Developer and Project Manager</h4>
                        <h5>Gomay Group Inc.</h5>
                        <p>• Led a team of six engineers on AngularJs code reviews, internal infrastructure, and process enhancements. </p>
                        <p>• Regularly delivered high-quality, supportable, scalable apps using AngularJs,WordPress and Laravel.</p>
                    </div>

                    <div class="event">
                        <h3>Feb 2015 - Jun 2015</h3>
                        <h4>Full Stack Web Developer</h4>
                        <h5>Valsfer.com (Start-Up Company)</h5>
                    </div>

                    <div class="event">
                        <h2>EDUCATION</h2>
                        <p>
                            <i class="pe-7s-study"></i>
                        </p>
                    </div>

                    <div class="event">
                        <h3>2013-2015</h3>
                        <h4>Master Science in Computer Science</h4>
                        <h5>Northwestern Polytechnic University</h5>
                        <p>GPA: 3.9</p>
                    </div>

                    <div class="event">
                        <h2>WORK HISTORY IN CHINA</h2>
                        <p>
                            <i class="pe-7s-ribbon"></i>
                        </p>
                    </div>

                    <div class="event">
                        <h3>2011 - 2013</h3>
                        <h4>Full Stack Web Developer & Project Manager</h4>
                        <h5>Beijing Yiyou Technology co., ltd (Start-Up Company)</h5>
                        <p>Leading the development team, manage project scope and deliverable to support business goals in collaboration</p>
                    </div>


                    <div class="event">
                        <h3>Oct 2005 – May 2010</h3>
                        <h4>IT Manager</h4>
                        <h5>Aoji Education Group</h5>
                        <p>• Promoted to IT Manager to managed a team of programmers and designers to build the first Aoji Education Group website.</p>
                        <p>• Collaborated heavily with cross-functional operations and executive stakeholders on all initiatives.</p>
                    </div>


                    <div class="event">
                        <h3>May 2001 – Oct 2005</h3>
                        <h4>Web Designer</h4>
                        <h5>Aoji Education Group</h5>
                        <p>• Executing all visual design stages from concept to final hand-off to engineering.</p>
                    </div>


                    <p><a href="/May-Resume-032020.pdf" class="button" target="_blank"><i class="pe-7s-download"></i>Download CV</a></p>

                </div>
                <!-- col -->

            {{-- SKILLS --}}
            <!-- col -->
                <div class="col-sm-5">

                    <!-- section-title -->
                    <div class="section-title center">
                        <h2>
                            <i>coding skills</i>
                        </h2>
                    </div>
                    <!-- section-title -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>React</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Vue.js 2</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->
                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>PHP</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Laravel</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Wordpress</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>SQL ( MySQL & Oracle )</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>HTML5</h4>
                        <div class="bar" data-percent="100">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>CSS3</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Unit Test</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->


                    <!-- section-title -->
                    <div class="section-title center">
                        <h2>
                            <i>UI / UX Tool skills</i>
                        </h2>
                    </div>
                    <!-- section-title -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Sketch App</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Figma</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->
                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Zeplin</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->


                    <!-- section-title -->
                    <div class="section-title center">
                        <h2>
                            <i>Other Tech Skills</i>
                        </h2>
                    </div>
                    <!-- section-title -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Git & Bitbucket</h4>
                        <div class="bar" data-percent="100">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Jira + Trello + Slack</h4>
                        <div class="bar" data-percent="100">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Wechat Platform Development</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Webpack</h4>
                        <div class="bar" data-percent="80">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Google Analytics</h4>
                        <div class="bar" data-percent="100">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Linux</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>

                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Amazon AWS</h4>
                        <div class="bar" data-percent="90">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->
                    <hr>
                    <!-- .skill-unit -->
                    <div class="skill-unit">
                        <h4>Never Stop Learning ...</h4>
                        <div class="bar" data-percent="100">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <!-- .skill-unit -->
                </div>
                <!-- col -->
            </div>
            <!-- row -->
        </div>
        <!-- .layout-medium -->
    </div>
    <!-- .content -->
    {{-- arrow --}}
    <div class="rel">
        <div class="left-arrow">
            <a href="/#/portfolio">
                @lang('resume.portfolio')
            </a>
        </div>
        <div class="right-arrow">
            <a href="/#/contact">
                @lang('resume.contact')
            </a>
        </div>
    </div>
    {{--/ arrow --}}
</section>
