<section id="portfolio" class="pt-page page-layout portfolio">
  <!-- .content -->
  <div class="content">
    <!-- .layout-medium -->
    <div class="layout-medium">
      <!-- page-title -->
      <h1 class="page-title">
        <i class="pe-7s-glasses"></i>some of my works
      </h1>
      <!-- page-title -->
      <!--FILTERS-->
      <ul id="filters" class="filters">
        <li class="current">
          <a href="#" data-filter="*">All</a>
        </li>
        <li>
          <a href="#" data-filter=".ux">UX / Protypes</a>
        </li>
        <li>
          <a href="#" data-filter=".web">Web / APP</a>
        </li>
        <li>
          <a href="#" data-filter=".sys">System</a>
        </li>
      </ul>
      <!--FILTERS-->
      <!-- PORTFOLIO -->
      <div class="portfolio-items media-grid masonry" data-layout="masonry" data-item-width="340">

          <div class="media-cell web url sys hentry">
              <div class="media-box">
                  <img src="/resume/images/portfolio/16_ticket_plus.jpg" alt="portfolio-post">
                  <div class="mask"></div>
                  <a href="//ticket.imay.us/" target="_blank"></a>
              </div>
              <div class="media-cell-desc">
                  <h3>ticket.imay.us 2019.3</h3>
                  <p class="category">RWD (Laravel + Vue + SQLite)</p>
                  <p>username: ashucn@gmail.com</p>
                  <p>password: 88888888</p>
              </div>
          </div>
          <!-- portfolio-item -->

          <div class="media-cell web url hentry">
              <div class="media-box">
                  <img src="/resume/images/portfolio/10.jpg" alt="portfolio-post">
                  <div class="mask"></div>
                  <a href="//hotel.usitour.com" target="_blank"></a>
              </div>
              <div class="media-cell-desc">
                  <h3>hotel.usitour.com</h3>
                  <p class="category">RWD (Laravel + Vue)</p>
              </div>
          </div>
          <!-- portfolio-item -->
          <!-- portfolio-item - custom url -->
          <div class="media-cell ux sys img hentry">
              <div class="media-box">
                  <img src="/resume/images/portfolio/11.jpg" alt="portfolio-post">
                  <div class="mask"></div>
                  <a href="/resume/images/portfolio/11-2.jpg" target="_blank"></a>
              </div>

              <div class="media-cell-desc">
                  <h3>booking system UX design</h3>
                  <p class="category">(Prototype)</p>
              </div>

          </div>
          <!-- portfolio-item -->
          <!-- portfolio-item - custom url -->
          <div class="media-cell sys img hentry">
              <div class="media-box">
                  <img src="/resume/images/portfolio/15.jpg" alt="portfolio-post">
                  <div class="mask"></div>
                  <a href="http://www.117book.com" target="_blank"></a>
              </div>

              <div class="media-cell-desc">
                  <h3>B2B Hotel Affiliate System</h3>
                  <p class="category">(Java + Laravel + Vue)</p>
              </div>

          </div>
          <!-- portfolio-item -->
          <!-- portfolio-item - custom url -->
          <div class="media-cell web sys img">
              <div class="media-box">
                  <img src="/resume/images/portfolio/12.jpg" alt="portfolio-post">
                  <div class="mask"></div>
                  <a href="/resume/images/portfolio/12-2.jpg" target="_blank"></a>
              </div>

              <div class="media-cell-desc">
                  <h3>ebooking.117book.com</h3>
                  <p class="category">(Laravel + Vue)</p>
              </div>

          </div>
          <!-- portfolio-item -->
        <!-- portfolio-item -->
        <!-- portfolio-item - custom url -->
        <div class="media-cell web url hentry">
          <div class="media-box">
            <img src="/resume/images/portfolio/01.jpg" alt="portfolio-post">
            <div class="mask"></div>
            <a href="http://www.3fenban.com/" target="_blank"></a>
          </div>

          <div class="media-cell-desc">
            <h3>330 Musician</h3>
            <p class="category">Musician Platform</p>
          </div>

        </div>
        <!-- portfolio-item -->

        <!-- portfolio-item -->
        <!-- portfolio-item - custom url -->
        <div class="media-cell ux sys url hentry">
          <div class="media-box">
            <img src="/resume/images/portfolio/02.jpg" alt="portfolio-post">
            <div class="mask"></div>
            <a href="https://www.figma.com/file/JHrl9w1BGcXZMz8g9941j2Kc/%E9%9C%93%E8%A3%B3CRM%26OA-(1-3%E6%A8%A1%E5%9D%97-%E5%8A%9F%E8%83%BD%E8%AE%BE%E8%AE%A1%E5%8F%8APrototype)" target="_blank"></a>
          </div>

          <div class="media-cell-desc">
            <h3>Newvogue Beauty Spa</h3>
            <p class="category">OA & CRM (Prototype)</p>
          </div>

        </div>
        <!-- portfolio-item -->

        <!-- portfolio-item - custom url -->
        <div class="media-cell web url hentry">

          <div class="media-box">
            <img src="/resume/images/portfolio/03.jpg" alt="portfolio-post">
            <div class="mask"></div>
            <a href="http://www.newvoguebeauty.com" target="_blank"></a>
          </div>

          <div class="media-cell-desc">
            <h3>Newvogue Beauty Spa</h3>
            <p class="category">Spa Beauty Website</p>
          </div>

        </div>
        <!-- portfolio-item - custom url -->

        <!-- portfolio-item - custom url -->
        <div class="media-cell web url hentry">
          <div class="media-box">
            <img src="/resume/images/portfolio/04.jpg" alt="portfolio-post">
            <div class="mask"></div>
            <!-- lightbox - image -->
            <a href="http://en.gomaygroup.com" target="_blank"></a>
            <!-- lightbox - image -->
          </div>
          <div class="media-cell-desc">
            <h3>Gomay Group</h3>
            <p class="category">Advertising Agency (Wordpress)</p>
          </div>
        </div>

        <div class="media-cell sys url hentry">
          <div class="media-box">
            <img src="/resume/images/portfolio/07.jpg" alt="portfolio-post">
            <div class="mask"></div>
            <a href="https://github.com/ashucn/laravel_realtime_chatroom" target="_blank"></a>
          </div>
          <div class="media-cell-desc">
            <h3>Real-Time Chatroom</h3>
            <p class="category">App Demo (Laravel+Vue+Pusher)</p>
          </div>
        </div>

      </div>
      <!-- PORTFOLIO -->

    </div>
    <!-- .layout-medium -->
  </div>
    {{-- arrow --}}
  <div class="rel">
    <div class="left-arrow">
    <a href="/#/about">
        @lang('resume.aboutme')
        </a>
    </div>
    <div class="right-arrow">
    <a href="/#/resume">
        @lang('resume.resume')
        </a>
    </div>
</div>
{{--/ arrow --}}
  <!-- .content -->
</section>
