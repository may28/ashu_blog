<section id="about" class="pt-page page-layout">
    <!-- .content -->
    <div class="content">
        <!-- .layout-medium -->
        <div class="layout-medium">

            <!-- page-title -->
            <h1 class="page-title">
                <i class="pe-7s-user"></i>@lang('resume.aboutme')
            </h1>
            <h2>
                The skill I'm proud the most is the ability to building websites and web apps from scratch.
            </h2>
            <p class="about_text">
                <strong>10+</strong> years experience in Web development and Web UI/UX design.
                <strong>4+</strong> years experience in startup environment.
            <div><strong>- Expert</strong> in JavaScript, HTML, CSS, ReactJs, VueJs, Sketch App, Figma, Adobe XD</div>
            <div>
                <strong>- Hands-on</strong> experience with various software development tools, including Create React App, Material-UI, Ant Design, Laravel(PHP), Wordpress,
                WebStorm IDE,
                Visual
                Studio Code, MySQL, AWS, GraphQL, Oracle VirtualBox, Vagrant, Linux, SSH, GitHub
            </div>
            <div>
                Organized, highly motivated, fast learner and a very quick problem solver. Diligent worker and a strong team player for any organization.
            </div>

            <!-- page-title -->

            <!-- section-title -->
            <div class="section-title center">
                <h2>
                    <i>Skills</i>
                </h2>
            </div>
            <!-- section-title -->

            <!-- row -->
            <div class="row">

                <!-- col -->
                <div class="col-sm-6 col-md-6">

                    <!-- service -->
                    <div class="service">
                        <!--<i class="pe-7s-glasses"></i>-->
                        <img src="/resume/images/site/icon-02.png" alt="image"/>
                        <h3>Web Development</h3>
                        <h4>Building websites and web apps from scratch. From Front-End to Back-End.</h4>
                        <P>Specialties: <strong>Laravel</strong>(PHP), <strong>Vue</strong>(JavaScript) , MySQL development and Linux</P>
                    </div>
                    <!-- service -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-sm-6 col-md-6">

                    <!-- service -->
                    <div class="service">
                        <!--<i class="pe-7s-joy"></i>-->
                        <img src="/resume/images/site/icon-01.png" alt="image"/>
                        <h3>UI/UX Design</h3>
                        <h4>Designing the wireframes of product, establishing creative solutions and crafting compelling user experiences</h4>
                        <P>Specialties: <strong>Sketch App</strong>, Zeplin, Adobe XD, Figma</P>
                    </div>
                    <!-- service -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-sm-6 col-md-6">

                    <!-- service -->
                    <div class="service">
                        <!--<i class="pe-7s-rocket"></i>-->
                        <img src="/resume/images/site/icon-03.png" alt="image"/>
                        <h3>R&D Management</h3>
                        <h4>Guarantee and improve product consistency and quality, deliver the right quality</h4>
                        <P>Specialties: <strong>Google Analitics</strong>, SEO（hotjar, ahrefs, etc.）</P>
                    </div>
                    <!-- service -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-sm-6 col-md-6">

                    <!-- service -->
                    <div class="service">
                        <!--<i class="pe-7s-scissors"></i>-->
                        <img src="/resume/images/site/icon-04.png" alt="image"/>
                        <h3>Problem Solver</h3>
                        <h4>Not only am I a problem solver – I also solve problems correctly with my years of experiences. </h4>
                    </div>
                    <!-- service -->

                </div>
                <!-- col -->

            </div>
            <!-- row -->
            <!-- SERVICES -->


            <!-- PROCESS -->

            <!-- section-title -->
            <div class="section-title center">
                <h2>
                    <i>work procces</i>
                </h2>
            </div>
            <!-- section-title -->


            <!-- row -->
            <div class="row last-row">

                <!-- col -->
                <div class="col-xs-4 col-sm-2">

                    <!-- process -->
                    <div class="process">
                        <i class="pe-7s-chat"></i>
                        <!--<img src="/resume/images/site/icon-03.png" alt="image"/>-->
                        <h4>DISCOVER</h4>
                    </div>
                    <!-- process -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-xs-4 col-sm-2">

                    <!-- process -->
                    <div class="process">
                        <i class="pe-7s-light"></i>
                        <h4>IDEA</h4>
                    </div>
                    <!-- process -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-xs-4 col-sm-2">

                    <!-- process -->
                    <div class="process">
                        <i class="pe-7s-vector"></i>
                        <h4>DESIGN</h4>
                    </div>
                    <!-- process -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-xs-4 col-sm-2">

                    <!-- process -->
                    <div class="process">
                        <i class="pe-7s-network"></i>
                        <h4>DEVELOP</h4>
                    </div>
                    <!-- process -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-xs-4 col-sm-2">

                    <!-- process -->
                    <div class="process">
                        <i class="pe-7s-browser"></i>
                        <h4>TEST</h4>
                    </div>
                    <!-- process -->

                </div>
                <!-- col -->

                <!-- col -->
                <div class="col-xs-4 col-sm-2">

                    <!-- process -->
                    <div class="process">
                        <i class="pe-7s-rocket"></i>
                        <h4>LAUNCH</h4>
                    </div>
                    <!-- process -->

                </div>
                <!-- col -->

            </div>
            <!-- row -->
            <!-- PROCESS -->

        </div>
        <!-- .layout-medium -->
    </div>
    <!-- .content -->
    {{-- arrow --}}
    <div class="rel">
        <div class="left-arrow">
            <a href="/#/home">
                @lang('resume.home')
            </a>
        </div>
        <div class="right-arrow">
            <a href="/#/resume">
                @lang('resume.resume')
            </a>
        </div>
    </div>
    {{--/ arrow --}}
</section>
