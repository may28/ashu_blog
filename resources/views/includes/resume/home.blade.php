<section id="home" class="pt-page page-layout light-text home-section has-bg-img"
         style="background-image:url(/resume/images/bg_user.jpg)">
  <!-- .content -->
  <div class="content">
    <!-- .layout-medium -->
    <div class="layout-medium">
      <h4>Hi, @lang('resume.iam')</h4>
      <h2>@lang('resume.name')</h2>
      <h4>@lang('resume.an')<strong id="typist-element" data-typist="@lang('resume.homeinfo2')">@lang('resume.homeinfo1')</strong></h4>
    </div>
    <!-- .layout-medium -->
  </div>
  <!-- .content -->
  {{-- arrow --}}
  <div class="rel height-100">
    <div class="right-arrow">
    <a href="/#/about">
        @lang('resume.aboutme')
        </a>
    </div>
</div>
{{--/ arrow --}}

<!-- .content -->
</section>

