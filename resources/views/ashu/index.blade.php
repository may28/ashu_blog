<!DOCTYPE html>
<html lang="en" class="no-js one-page-layout" data-mobile-classic-layout="false" data-classic-layout="false"
      data-prev-animation="16" data-next-animation="15" data-random-animation="false">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta name="description" content="May's Online Resume">
  <meta name="keywords" content="Ashucn, May Liu, Resume, portfolio">
  <meta name="author" content="May">
  <title>May's Resume</title>
  <!-- FAV and TOUCH ICONS -->
  <link rel="shortcut icon" href="/resume/images/avatar.jpg">
  <link rel="apple-touch-icon" href="/resume/images/avatar.jpg"/>
  <!-- FONTS -->
  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic' rel='stylesheet'
        type='text/css'>
  <!-- STYLES -->
    <link rel="stylesheet" href="{{ mix('resume/css/vendor.css') }}"/>

  <!-- INITIAL SCRIPTS -->
    <script src="{{ mix('resume/js/vendor-init.js') }}" type="text/javascript"></script>
  <!--[if lt IE 9]>
  <script src="/resume/js/ie.js"></script>
  <![endif]-->
</head>
<body>
<!-- PAGE -->
<div id="page" class="hfeed site">
  <!-- HEADER -->
  <header id="masthead" class="header" role="banner">
    <a class="menu-toggle toggle-link"></a>
    <h1 class="site-title mobile-title">May LIU</h1>
    <!-- header-wrap -->
    <div class="header-wrap">
      <!-- NAV MENU -->
    @include('includes.resume.nav')
    <!-- NAV MENU -->
    </div>
  </header>

  <!-- .site-main -->
  <div id="main" class="site-main">
    <!-- PAGE : HOME -->
    @include('includes.resume.home')
    <!-- PAGE : ABOUT -->
    @include('includes.resume.about')
    <!-- PAGE : ABOUT -->
    @include('includes.resume.portfolio')
    <!-- PAGE : RESUME -->
    @include('includes.resume.resume')
    <!-- PAGE : CONTACT -->
   @include('includes.resume.contact')
  </div>
  <!--/ .site-main -->
</div>
<!--/ PAGE -->

<!-- PORTFOLIO SINGLE AJAX CONTENT CONTAINER -->
{{-- <div class="p-overlay"></div>
<div class="p-overlay"></div> --}}

<!-- ALERT : used for contact form mail delivery alert -->
<div class="site-alert animated"></div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNQaIjvsoKuOyhDjx_OnOWV7fHxhO9C4U"></script>

<!-- SCRIPTS -->
<script src="{{ mix('resume/js/vendor.js') }}" type="text/javascript"></script>
{{-- <script src="/resume/js/main.js"></script> --}}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109419428-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109419428-1');
</script>

</body>
</html>
